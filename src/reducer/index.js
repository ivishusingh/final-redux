import { combineReducers } from "redux";
const initialState = {
  Data: [
    { name: "vishu", email: "abc@xyz.com" },
    { name: "Elina", email: "elina@vampire.com" }
  ]
};

const Add_Reducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_USER":
      return {
        ...state,
        Data: state.Data.concat(action.payload)
      };
    case "DEL_USER":
      let res = [...state.Data];
      let updated = res.filter((item, i) => action.payload !== i);
      return {
        ...state,
        Data: updated
      };
    case "EDIT_USER":
      let final = [...state.Data];
      final.splice(action.index, 1, action.payload);

      return {
        Data: final
      };
    default:
      return state;
  }
};

const showReducer = (state = [], action) => {
  switch (action.type) {
    case "SHOW_DETAILS":
      return action.payload;
    default:
      return state;
  }
};

export default combineReducers({ Add_Reducer, showReducer });
