export function AddUser(name, email, password) {
  return {
    type: "ADD_USER",
    payload: { name: name, email: email, password: password }
  };
}
export function DelUser(i) {
  return {
    type: "DEL_USER",
    payload: i
  };
}
export function ShowUser(user, i) {
  return {
    type: "SHOW_DETAILS",
    payload: { user, i }
  };
}
export function EditUser(name, email, password, i) {
  return {
    type: "EDIT_USER",
    index: i,
    payload: { name, email, password, i }
  };
}
