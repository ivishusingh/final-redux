import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./components/Home";
import Adduser from "./components/Adduser";
import List from "./components/List";
import Showuser from "./components/Showuser";

import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/add" component={Adduser} />
            <Route exact path="/list" component={List} />
            <Route exact path="/show" component={Showuser} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
