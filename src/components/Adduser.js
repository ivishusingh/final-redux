import React, { Component } from "react";
import Navbar from "./Navbar";
import { AddUser } from "../action";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Adduser extends Component {
  state = {
    name: "",
    email: "",
    password: ""
  };
  validate=()=>{
    let error=false

if(this.state.name.length <3){
error=true
alert('name should be atleast 5 char long ')
}
if(this.state.email.indexOf('a')===-1){
  error=true
  alert('email should be atleast 5 char long ')
  }

  }
  AddItem = () => {
    const err=this.validate();
    if(!err){
    this.props.AddUser(
      this.state.name, this.state.email, this.state.password);
  };}
  render() {
    return (
      <div>
        <Navbar />

        <div className="row mt-5">
          <div className="col  ">
            <div style={{ width: "50rem" }} className="card">
              <div className="card-body">
                <form>
                  <div className="form-group">
                    <label>Email address</label>
                    <input
                      onChange={e => {
                        this.setState({ email: e.target.value });
                      }}
                      required
                      type="email"
                      className="form-control"
                      aria-describedby="emailHelp"
                      placeholder="Enter email"
                    />
                  </div>
                  <div className="form-group">
                    <label>Name</label>
                    <input
                      onChange={e => {
                        this.setState({ name: e.target.value });
                      }}
                      type="text"
                      className="form-control"
                      placeholder="name"
                    />
                  </div>
                  <div className="form-group">
                    <label>Password</label>
                    <input
                      onChange={e => {
                        this.setState({ password: e.target.value });
                      }}
                      type="password"
                      className="form-control"
                      placeholder="Password"
                    />
                  </div>
                  <div className="form-group">
                    <label> Confirm Password</label>
                    <input
                      onChange={e => {
                        this.setState({ password: e.target.value });
                      }}
                      type="password"
                      className="form-control"
                      id="exampleInputPassword1"
                      placeholder="Confirm Password"
                    />
                  </div>

                  <Link to="/list">
                    <button
                      onClick={this.AddItem}
                      className="btn btn-outline-primary"
                    >
                      Add user
                    </button>
                  </Link>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state;
};

export default connect(
  mapStateToProps,
  { AddUser }
)(Adduser);
