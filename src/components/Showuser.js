import React, { Component } from "react";
import { ShowUser, DelUser, EditUser } from "../action";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Navbar from './Navbar'

class Showuser extends Component {
  state = {
    showInput: false,
    name: "",
    email: "",
    password: "",
    index: this.props.i
  };
  update = () => {
    this.setState({ showInput: false });
    this.props.EditUser(
      this.state.name,
      this.state.email,
      this.state.password,
      this.state.index
    );
    alert("updated succesfully");
  };
  editinput = () => {
    this.setState({ showInput: true });
  };
  render() {
    return (
      <div>
        <Navbar/>
        <div style={{ width: "78rem" }} class="card">
          <div className="card-header">User Info</div>
          <div className="card-body">
            <h5 className="card-title">Name:{this.props.user.name}</h5>
            <h5 className="card-title">Email:{this.props.user.email}</h5>
            <p className="card-body">password:{this.props.user.password}</p>
            {this.state.showInput ? (
              <div>
                <form>
                  <div className="form-group">
                    <label>Email address</label>
                    <input
                      onChange={e => this.setState({ email: e.target.value })}
                      required
                      type="email"
                      className="form-control"
                      aria-describedby="emailHelp"
                      placeholder="Enter email"
                    />
                  </div>
                  <div className="form-group">
                    <label>Name</label>
                    <input
                      onChange={e => this.setState({ name: e.target.value })}
                      required
                      type="text"
                      className="form-control"
                      placeholder="name"
                    />
                  </div>
                  <div className="form-group">
                    <label>Password</label>
                    <input
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                      required
                      type="password"
                      className="form-control"
                      placeholder="Password"
                    />
                  </div>
                  <div className="form-group">
                    <label> Confirm Password</label>
                    <input
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                      required
                      type="password"
                      className="form-control"
                      id="exampleInputPassword1"
                      placeholder="Confirm Password"
                    />
                  </div>
                  <Link to="/list">
                    <button
                      onClick={() => this.update(this.props.i)}
                      className="btn btn-outline-primary"
                    >
                      Update
                    </button>
                  </Link>
                </form>
              </div>
            ) : null}

            <Link to="/list">
              <button
                onClick={() => this.props.DelUser(this.props.i)}
                className="btn btn-outline-danger m-1"
              >
                Delete
              </button>
            </Link>

            <button
              onClick={this.editinput}
              className="btn btn-outline-info m-1"
            >
              Edit
            </button>
          </div>
        </div>
        <Link to="/list">
          <button className="btn btn-outline-warning m-2">Show List</button>
        </Link>
        <Link to="/add">
          <button className="btn btn-outline-success m-2">Add New User</button>
        </Link>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state.showReducer;
};
export default connect(
  mapStateToProps,
  { ShowUser, DelUser, EditUser }
)(Showuser);
