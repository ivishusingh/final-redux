import React, { Component } from "react";
import Navbar from "./Navbar";
import { connect } from "react-redux";
import { ShowUser } from "../action";
import { Link } from "react-router-dom";

class List extends Component {
  render() {
    return (
      <div>
        <Navbar />

        <div>
          <table className="table">
            <thead className="thead-dark">
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.props.Data && this.props.Data.length > 0
                ? this.props.Data.map((item, i) => (
                    <tr key={i}>
                      <td>{item.name}</td>
                      <td>{item.email}</td>
                      <td>
                        <Link to="/show">
                          <button
                            onClick={() => this.props.ShowUser(item, i)}
                            className="btn btn-success"
                          >
                            View
                          </button>
                        </Link>
                      </td>
                    </tr>
                  ))
                : null}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  let propState = state.Add_Reducer;
  return propState;
};
export default connect(
  mapStateToProps,
  { ShowUser }
)(List);
