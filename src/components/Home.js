import React from "react";
import { NavLink } from "react-router-dom";
import Navbar from "./Navbar";

export default function Home() {
  return (
    <div>
      <Navbar />
      <div className="row mt-5">
        <div className="col text-center ">
          <h3>Welcome To Crud-Redux App</h3>
          <NavLink to="/add">
            <button className="btn btn-outline-success m-2">Add New</button>
          </NavLink>

          <NavLink to="/list">
            <button className="btn btn-outline-info m-2">Show list</button>
          </NavLink>
        </div>
      </div>
    </div>
  );
}
